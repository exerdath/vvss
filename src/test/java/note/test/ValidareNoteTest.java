package note.test;

import note.model.Nota;
import note.repository.NoteRepositoryMock;
import note.utils.ClasaException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Exerdath on 27-Mar-18.
 */
public class ValidareNoteTest {

    @Test
    public void ECP01() throws ClasaException {
        NoteRepositoryMock noteRepositoryMock=new NoteRepositoryMock();
        int b4TestNrNotes=noteRepositoryMock.nrNote();
        noteRepositoryMock.addNota(new Nota(123,"RobiTes",6.7));
        int afterTestNrNote=noteRepositoryMock.nrNote();
        Assert.assertEquals(b4TestNrNotes,afterTestNrNote-1);

    }
    @Test(expected = ClasaException.class)
    public void ECP02() throws ClasaException {
        NoteRepositoryMock noteRepositoryMock=new NoteRepositoryMock();
        noteRepositoryMock.addNota(new Nota(123,"Rob",6.7));
    }

    @Test(expected = ClasaException.class)
    public void ECP03() throws ClasaException {
        NoteRepositoryMock noteRepositoryMock=new NoteRepositoryMock();
        noteRepositoryMock.addNota(new Nota(-2,"Rob",6.7));
    }

    @Test
    public void BVA01() throws ClasaException {
        NoteRepositoryMock noteRepositoryMock=new NoteRepositoryMock();
        int b4TestNrNotes=noteRepositoryMock.nrNote();
        noteRepositoryMock.addNota(new Nota(123,"RobiTes",10.0));
        int afterTestNrNote=noteRepositoryMock.nrNote();
        Assert.assertEquals(b4TestNrNotes,afterTestNrNote-1);
    }

    @Test(expected = ClasaException.class)
    public void BVA02() throws ClasaException {
        NoteRepositoryMock noteRepositoryMock=new NoteRepositoryMock();
        noteRepositoryMock.addNota(new Nota(123,"RobiTes",10.1));
    }

    @Test(expected = ClasaException.class)
    public void BVA03() throws ClasaException {
        NoteRepositoryMock noteRepositoryMock=new NoteRepositoryMock();
        noteRepositoryMock.addNota(new Nota(-1,"RobiTes",1.2));
    }





}
